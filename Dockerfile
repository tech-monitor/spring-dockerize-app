FROM openjdk:11

WORKDIR /app
COPY ./target/spring-dockerize-app-0.0.1-SNAPSHOT.jar /app

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "spring-dockerize-app-0.0.1-SNAPSHOT.jar"]