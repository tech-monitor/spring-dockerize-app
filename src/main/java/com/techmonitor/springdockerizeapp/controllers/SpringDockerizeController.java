package com.techmonitor.springdockerizeapp.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringDockerizeController {
	
	
	@GetMapping("/ping")
	public String ping() {
		return "SpringDockerizeController ping ...."; 
	}
	

}
