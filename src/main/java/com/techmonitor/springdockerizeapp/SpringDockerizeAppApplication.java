package com.techmonitor.springdockerizeapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDockerizeAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDockerizeAppApplication.class, args);
	}

}
